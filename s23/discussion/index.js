//console.log("Hello Jesus"`);


// [Section] Objects - data type that is used to represent real world objects.
//		- it is a collection of related data and/or functionalities

// Structure of Objects:
// 		key - is the where call the property of an object
// 		value - is the value of the specific key or property

// Creating using the object initializers/ object literal notation

/*
	Syntax:
		let objectName = {
			keyA: valueA;
			keyB: valueB;
		}
*/

let cellphone = {
	// key-value pairs
	name: "Nokia 3210",
	manufactureDate: 1999
}

console.log("Result from creating objects using initializers/literal notation:")
let exampleArray = [1,2,3,4,5];
console.log(exampleArray);
console.log(typeof exampleArray);

console.log(cellphone);
console.log(typeof cellphone);
console.log(cellphone.length);

// Creating objects using constructor function
// 		creates a reusable function to create several objects that have the same data structure
// 	This useful for creating multiple instances/copies of an objects
// An instance is a concrete occurance which emphasizes on the distinct/unique identity of it
/*
	Syntax:
		function ObjectName(keyA,keyB){
			this.keyA = valueA;
			this.keyB = valueB;
		}
*/

function Laptop(name, manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
}
// This is a unique instance of the Laptop object
// 		- "new" operator creates an instance of an object
// 		- Objects and instances are often interchanged because object literal (let object)
// 		-{} and instances (let object = new object) are distinct/unique object

let laptop = new Laptop('Lenovo', 2008);
console.log("Result from creating objects using objects constructors: ");
console.log(laptop);

// this is another unique instance of the Laptop object
let myLaptop = new Laptop("Macbook Air", 2020);
console.log("Result from creating objects using objects constructors: ");
console.log(myLaptop);

let oldLaptop = new Laptop("Portal R2E","1980");
// - The example above invokes/calls the "Laptop" instead of creating a new object instance
// - Returns "undefined" without the "new" operator because the "Laptop" function does not have a return statement

console.log("Result from creating objects using objects constructors: ");
console.log(oldLaptop);

// Creating empty object
let computer = {};
let myComputer = new Object();

// [Section] Accessing Object Properties

// Using the dot notation
console.log("Result from dot notation: "+ myLaptop.name);

// Using Square bracket notation
console.log("Result from dot notation: "+ myLaptop['name']);
console.log("Result from dot notation: "+ myLaptop['manufactureDate']);

// Accessing array objects
//  - Accessing array elements can also be done using square brackets
//  - Accessing object properties using the square bracket notation and array indexes can cause confusion
// 	- By using dot notation, this easily helps us differentiate accessing elements for arrays and properties from objects
//  - Object properties have names that make it easier to associate pieces of information

let array = [laptop, myLaptop]

console.log(array[0]["name"]);

console.log(array[0].name);

console.log(array[1].name);
console.log(array[1].manufactureDate);

//[Section] Initializing/Adding/Deletring/Reassigning Objects Properties
// 	- like any other variable in JavaScript, objects may have their properties intialized/ add after the object was created/declared
//  - This is usefull for times when an object's properties are undetermined at the time of creating them

let car = "Honda Civic";
// initializing/adding object properties using dot notation
console.log("Result from adding properties using dot notation");
console.log(car);

car['manufacture date'] = 2019;
console.log(car['manufacture date']);
console.log(car['Manufacture Date']);
console.log(car.manufactureDate);
console.log("Result from adding properties using square bracket notation: ");
console.log(car);

// Deleting object properties
delete car["manufacture date"];
console.log("Result from deleting properties");
console.log(car);

// Reassigning object properties
car.name = "Dodge Charge R/T"
console.log("Result from deleting properties");
console.log(car);

// [Section] Object Methods
// 	- A method is a function which is property of an object
// 	- They are also functions and one of key differences they have is that methods are functions related ti a specific object
// 	- Methofs are u usefull for creating object specific function which iare used to perform task on them
//  - SImilar to function/ features of real world object, methos are defined based on what an object is cableof doing how it works

let person = {
	name: "John",
	talk: function(){
		console.log("Hello my name is "+ this.name);
	}
}
console.log(person);
console.log("Result from object methods: ");
person.talk();

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function()	{
		console.log("This pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduce to <targetPokemon>");
						},
	faint: function()	{
		console.log("Pokemon fainted");
						}
				}
console.log(myPokemon);

function Pokemon(name, level)	{
	//properties
	this.name = name;
	this.level = level;
	this.health = 2*level;
	this.attack = level
	//methods
	this.tackle = function(target)	{
		console.log(this.name+" tackled "+target.name);
		console.log("targetPokemon's health is now reduce to <targetPokemonhealth");
									};
	this.faint = function()			{
		console.log(this.name+" fainted ");
									}

								}

let pikachu = new Pokemon("Pikachu",16);
let rattata = new Pokemon("Rattata",8);

pikachu.tackle(rattata);
const express = require("express");

const taskControllers = require("../controllers/taskControllers.js")

// Contain all the endpoints of our application
const router = express.Router();

router.get("/", taskControllers.getAllTasks);

router.post("/addTask", taskControllers.addTasks)


module.exports = router;

// Parameterized

// We are creata a route using a Delete method at the URL "/tasks/:id"
// the colon here is an identifier that helps to create a dynamic route which allows us to supply information

router.delete("/:id", taskControllers.deleteTask);

module.exports = router;

// Activity start here ///////////////////////////////////////

router.get("/:id", taskControllers.getSpecificTask);

router.put("/:id/complete", taskControllers.specificTaskComplete);
module.exports = router;
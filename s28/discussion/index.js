// show databases - list of the db inside our cluster
// use 'dbName' - to use specify database
// show collections - to see the list of collection of db

//CRUD operation - the heart of any backend application
//		-mastering the CRUD operation is essential for any developer especially to those who want to become backend developer

// [Section] Inserting Document (create)
// 		Insert one document - since mongoDB deals with objects as it's structure for documents we can easily create them by providing objects in our method/operation.
/*
	syntax:
		db.collectionName.insertOne({
			object(to be add)
		})
*/
db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "123456789",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS","JavaScript","Python"],
	department: "none"
})

//Insert many
/*
	syntax:
		db.collectionName.insertMany([{objectA},{objectB}]);
*/

db.users.insertMany([
		{
			firstname: "Stephen",
			lastName: "Hawking",
			age:76,
			contact:{
				phone: "87654321",
				email: "stephenhawking@gmail.com"
			},
			courses: ["Phyton","React","Php"],
			department : "none"
		},
		{
			firstName: "Neil",
			lastName: "Armstrong",
			age: 82,
			contact: {
				phone: "87654321",
				email: "neilarmstrong@gmail.com"
			},
			courses: ["React","Laravel","Sass"],
			department: "none"
		}
])

db.users.insertMany([
		{
			firstName: "Stephen",
			lastName: "Hawking",
			age:76,
			contact:{
				phone: "87654321",
				email: "stephenhawking@gmail.com"
			},
			courses: ["Phyton","React","Php"],
			department : "none"
		},
		{
			firstName: "Neil",
			lastName: "Armstrong",
			age: 82,
			contact: {
				phone: "87654321",
				email: "neilarmstrong@gmail.com"
			},
			courses: ["React","Laravel","Sass"],
			department: "none"
		}
])

db.userss.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "123456789",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS","JavaScript","Python"],
	department: "none"
})


// [Section] Finding documents (read operation)
/*
	db.collectionName.find();
	db.collectionName.find({field:value});
*/

db.users.find();
// using find() method, will show you the list of all the documments inside our collection
db.users.find().pretty();
// "pretty" methods allows use to be able to view the documents returned by or terminals to be in a better format
db.users.find({firstName:"Stephen"});
// it will return the documents that will pass the criteria given in the method

db.users.find({_id:"646c5b133e96867bdce95494"});
db.users.find({_id: ObjectId("646c5b133e96867bdce95494")});

// multiple criteria
db.users.find({lastName:"Armstrong",age:82});

db.users.find("contact.phone":"123456789");

//[Section]	Updating documents(update)
db.users.insertOne({
	firstName: "test",
	lastName: "test",
	age: 0,
	contact:{
		phone:"00000000",
		email:"test@gmail.com"
	},
	courses:[],
	department:"none"
})

// updateOne method
/*
	syntax:
	db.collectionName.updateOne({criteria},{$set:{field:value}});
*/

db.users.updateOne(
	{ firstName: "test"},
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age:65,
			contact: {
				phone: "12345678",
				email: "bill@gmail.com"
			},
			courses: ["PHP","Laravel","HTML"],
			department: "Operations",
			status: "none"
		}
	}

)

db.users.updateOne(
	{ firstName: "Bill"},
	{
		$set: {
			firstName: "Chris",
			
		}
	}
)

db.users.updateOne(
	{firstName: "Jane"},
	{
		$set:{
			lastName: "Edited"
		}
	}
	);

//Updating multiple documents
/*
	syntax:
	db.collectionName.updateMany(
		{criteria},
		{
			$set:{
				field:value
			}
		}
	)
*/

db.users.updateMany(
	{department: "none"},
	{
		$set:{
			department:"HR"
		}
	}

	)

db.users.updateMany(
	{department: "HR"},
	{
		$set:{
			
			school: "zuitt"
		}
	}

	)

//replace one
/*
	syntax:
	db.collectionName.replaceOne(
	{
	$set:{
		object
		}
	}
	)
*/
db.users.insertOne({firstName:"test"});

db.users.replaceOne(
		{firstName:"test"},
		
			{
				firstName:"Bill",
				lastName:"Gates",
				age:65,
				contact:[],
				courses: [],
				department: "Operations",
			}
			
		
	)

// [Section] deleting documents
//  deleting single documents
/*
	db.collectionName.deleteOne({criteria})
*/

db.users.deleteOne({firstName:"Bill"});

// delete multiple document
/*
	db.collectionName.deleteMany({criteria})
*/

db.users.deleteMany({firstName: "Jane"});

db.users.deleteMany({});
//delete all

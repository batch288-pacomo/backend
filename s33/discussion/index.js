// [Section] JavaScript Synchronous vs Asynchronous
// 		-Javascript is by default is synchronous meaning that only one statement is executed a time

/*console.log("Hello World!");

conole.log("Hello after the world");

console.log("Hello0");*/

/*for(let index = 0; index <= 10000; index++){
		console.log(index);
}
console.log("Hello again!");*/

// Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background

// [Section] Getting all posts
// 		- the Fetch API allows you to asynchronously request for a resource data
// 		- so it means the fetch method that we are going to use here will run asynchronously
/*
	syntax:
		fetch('URL');
*/
// "promise" - is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value
	console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

/*
	syntax:
		fetch('URL')
		.then(response => response);
*/

// Retrive all posts follow the REST API
	fetch('https://jsonplaceholder.typicode.com/posts')
// The Fetch method will return a promise that resolves the Response Object
// the "then" method captures the response object
// Use the "json" method from the Response object to be convert the data retrived into JSON format to be used in our application
	.then(response => response.json())
	.then(notjson => console.log(notjson))

/////////////////////////////////////////////////////////////////
// The "async" and "await" keyword, it is another approach that can be used to achieve asynchronous code

// Creates an asynchronous function
async function fetchData(){
	let result = await fetch('https://jsonplaceholder.typicode.com/posts');
	console.log(result);

	let json = await result.json();
	console.log(json);
};
//fetchData();



// [Section] Getting specific post
// Retrieves specific post following the rest API(/posts/:id)
fetch('https://jsonplaceholder.typicode.com/posts/5')
.then(response => response.json())
.then(json => console.log(json))

// [Section] Creating Post
/*
	syntax:
	 - option is an object that containes the method, the header and the body of the request
	 - by default if you dont add the method in the fetch request, it will be a GET method
		fetch('URL', options)
		.then(response => {})
		.then(response => {})
*/

fetch('https://jsonplaceholder.typicode.com/posts', {
	//sets the method of the request object to post following the rest API
	method: 'POST',
	//sets the header data of the request object to be sent to the backend
	//specified that the content will be in JSON structure
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
			title : 'New post',
			body : 'Hellow World',
			userId : 1
		})
})
.then(response => response.json())
.then(json => console.log(json));

// [Section] Update a specific post
//PUT method - is a method of modifying resource where the client sends data that updates the entire object/document
fetch('https://jsonplaceholder.typicode.com/posts/1'
	,{
		method: "PUT",
		headers: {
			'Content-Type':'application/json'
		},
		body: JSON.stringify({
					
					title: 'Updated Post',
					
				})

})
.then(response => response.json())
.then(json => console.log(json))

//PATCH method - applies a partial update to the object or document
fetch('https://jsonplaceholder.typicode.com/posts/1'
	,{
		method: "PATCH",
		headers: {
			'Content-Type':'application/json'
		},
		body: JSON.stringify({
					
					title: 'Updated Post',
					
				})

})
.then(response => response.json())
.then(json => console.log(json))

//[Section] Deleting a Post

fetch('https://jsonplaceholder.typicode.com/posts/1',{ method: "DELETE"})
.then(response => response.json())
.then(json => console.log())
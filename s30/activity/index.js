async function fruitsOnSale(db) {
	return await(

			db.fruits.aggregate([
					{$match:{onSale:true}},
					{$group:{
					 	_id:"$onSale",
					  	onSale:{$sum:"$stock"}
					}},{$project:{_id:0}}
				])

		);
};


async function fruitsInStock(db) {
	return await(

			db.fruits.aggregate([
					{$match:{stock:{$gte:20}}},
					{$group:{
					 	_id:"$onSale",
					  	morethan20:{$sum:"$stock"}
					}},{$project:{_id:0}}
				])
			

		);
};



async function fruitsAvePrice(db) {
	return await(

			db.fruits.aggregate([
					{$match:{onSale:true}},
					{$group:{
					 	_id:"$supplier_id",
					  	average:{$avg:"$price"}
					}}
				])

		);
};


async function fruitsHighPrice(db) {
	return await(

			db.fruits.aggregate([
					{$group:{
					 	_id:"$supplier_id",
					  	maxPrice:{$max:"$price"}
					}}
				])

		);
};



async function fruitsLowPrice(db) {
	return await(

			db.fruits.aggregate([
					{$group:{
					 	_id:"$supplier_id",
					  	minPrice:{$min:"$price"}
					}}
				])

		);
}


try{
    module.exports = {
        fruitsOnSale,
        fruitsInStock,
        fruitsAvePrice,
        fruitsHighPrice,
        fruitsLowPrice
    };
} catch(err){

};
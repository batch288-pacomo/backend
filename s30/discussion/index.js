db.fruits.insertMany([
  {
    name : "Apple",
    color : "Red",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "US" ]
  },

  {
    name : "Banana",
    color : "Yellow",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
    origin: [ "Philippines", "Ecuador" ]
  },

  {
    name : "Kiwi",
    color : "Green",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
    origin: [ "US", "China" ]
  },

  {
    name : "Mango",
    color : "Yellow",
    stock : 10,
    price: 120,
    supplier_id : 2,
    onSale : false,
    origin: [ "Philippines", "India" ]
  }
]);

//[Section] MongoDB aggregation - use to generate manipulated data and perform operations to create filtered results that helps anallyzing data.
//      - compare to doing the CRUD operations on our data from previous sessions, aggregation gives us access to manipulate, filter and compute for results providing us with information to make necessary development decisions without having to create a frontend application

// using aggregate method
db.fruits.aggregate([
    {$match:{onSale:true}}
    {$group: {_id:'$supplier_id', total:{
      $sum:"$stock"
    }}}
  ])

// match operator - 
db.fruits.aggregate([
    {$match: {onSale:true}}
  ])

// The $group operator - groups the documents in terms of the property declared in the _id property
// total - user defined, can be change
//sum - will add the value
// -id - determine the groupings
db.fruits.aggregate([
  {$group: {_id:'$supplier_id', total:{
      $sum:"$stock"
    }}}
  ])

// max/min operator
db.fruits.aggregate([
      {$match:{onSale:true}},
      {$group: {_id:'$supplier_id', 
        max:{$max:"$stock"},sum:{$sum:"$stock"}}
      }
  ])

db.fruits.aggregate([
    /*{$match:{onSale:true}},*/
    {$group:{_id:"$color",
    max:{$max:"$stock"},sum:{$sum:"$stock"}
    }}
  ])

db.fruits.aggregate([
    {$group:{_id:"$color",
    max:{$max:"$stock"},sum:{$sum:"$stock"}
    }}
  ])

// Field projection with aggregation
//    - $project - operator can be used when aggregating data to exclude(remove) the returned result

db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{
      _id:"$color",
      max:{$max:"$stock"},
      sum:{$sum:"$stock"}
    }},
    {$project:{_id:0}}
  ])

// Sorting aggregated result - $sort operator can be use to change the order of aggregated result
/*
    syntax:
      {$sort:{field:1/-1}}
*/
db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{
      _id:"$supplier_id",
      total:{$sum:"$stock"}
    }},
    {$sort:{total:1}} // positive(1) accending, negative(1) descending
  ])

db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{
      _id:"$name",
      stocks:{$sum:"$stock"}
    }},
    {$sort:{_id:1}} // alphabetical order
  ])

db.fruits.aggregate([
    {$group:{
      _id:"$onSale", // 2 groupings of (true/false)
      stocks:{$sum:"$stock"}
    }},
    {$sort:{_id:1}}
  ])


// 
db.fruits.aggregate([
  
  { $group : {
    _id : "$color",
    stocks : {$sum : "$stock"}
  }},
  { $project : { _id : 0}},
  {
    $sort : { _id : -1 }
  }
  ])


// aggregating results based on an array field
//    -$unwind operator - deconstructs an array field from a collection/field with an array value to output a result for each element
/*
    syntax:
        {$unwind:field}
*/

db.fruits.aggregate([
    {$unwind:"$origin"}
  ])

// Display fruit documents by their origin and the kinds of fruits that are supplied

db.fruits.aggregate([
    {$unwind:"$origin"},
    {$group:{
      _id:"$origin",
      kinds:{$sum:1} //{$sum:1} - it will count the number of documents in the group
    }}
  ])

db.fruits.aggregate([
    {$unwind:"$origin"},
    {$group:{
      _id:"$origin",
      kinds:{$sum:1}
    }},
    {$sort:{kinds:1}}
  ])


db.fruits.aggregate([
    {$unwind:"$origin"},
    {$group:{
      _id:"$origin",
      kinds:{$sum:1}
    }},
    {$sort:{kinds:1,_id:1}}//sort kinds,then sort id
  ])


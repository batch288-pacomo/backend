console.log("Hello Batch 288!");

// [Section] Arithmetic Operators
		let x = 1397;
		let y = 7831;
	// Addition Operator(+)
		let sum = x + y;
	console.log("Result of addition operator: "+sum);
	// Subtraction Operator(-)
		let difference = y - x;
	console.log("Result of Subtraction operator: "+difference);
	// Product Operator(*)
		let product = x * y;
	console.log("Result of multiplication operator: "+product);
	// Division(/)
		let quotient = y / x;
	console.log("Result of division operator: "+quotient);
	// Modulo Operator(%)
		let remainder = y % x;
	console.log("Result of modulo operator: "+remainder);

// [Section] Assignment Operators
	// Assignment Operator(=) - assigns/reassign the value to the variable
		let assignmentNumber = 8;
	// Addition Assignment Operator(+=) - adds the value of the right operand to a variable and assigns the result to the variable
		assignmentNumber += 2;
		console.log(assignmentNumber);
		assignmentNumber += 3;
		console.log("Result of addition assignment Operator: "+assignmentNumber);
	// Subtraction (-=)
		assignmentNumber -= 2;
		console.log("Result of subtraction assignment Operator: "+assignmentNumber);
	// /Multiplication (*=)
		assignmentNumber *= 3;
		console.log("Result of multiplication assignment Operator: "+assignmentNumber);
	// /Division (/=)
		assignmentNumber /= 11;
		console.log("Result of division assignment Operator: "+assignmentNumber);

// Multiple Operators and Parentheses
		// MDAS - multi or div first, then add or sub, from left to right
	let mdas = 1 + 2 - 3 * 4 / 5;
	/*
		3*4 = 12
		12/5 = 2.4
		1+2 = 3
		3-2.4 = 0.6
	*/
	console.log(mdas.toFixed(1));

	// PEMDAS - parenthesis,then multi or div first, then add or sub, from left to right
	let pemdas = 1 + (2-3) * (4/5);
	/*
		4/5 = 0.8
		2-3 = -1
		1 + (-1) * (0.8)
		-1 * 0.8 = -0.8
		1 -0.8 = 0.2
	*/
	console.log(pemdas.toFixed(1));

// [Section] Increment and Decrement
	// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment and decrement applied to

	// increment
		let z = 1;
		let increment = ++z;
		console.log("Result of pre-increment: "+increment);
		console.log("Result of pre-increment: "+z);

		increment = z++;
		console.log("Result of post-increment: "+increment);
		console.log("Result of post-increment: "+z);

	// decrement
		x = 1;
		let decrement = --x;
		console.log("Result of pre-decrement: "+decrement);
		console.log("Result of pre-decrement: "+x);

		decrement = x--;
		console.log("Result of post-decrement: "+decrement);
		console.log("Result of post-decrement: "+x);

// [Section] Type Coercion - is the automatic or implicit 			converion of values from one date type to another
		// -This happens when operations are performed on different data types that would normally possible and yield irregular results

		let numbA = '10';
		let numbB = 12;

		let coercion = numbA + numbB;
		// IF you are going to add string and number, it will concatenate its value TO STRING
		console.log(coercion);
		console.log(typeof coercion);

		let numC = 16;
		let numD = 14;

		let nonCoercion = numC + numD;
		console.log(nonCoercion);
		console.log(typeof nonCoercion);

		// Boolean "true" is also associated with the value of 1
		let numE = true + 1;
		console.log(numE);
		// Boolean "false" is also associated with the value of 0
		let numF = false + 1;
		console.log(numF);

// [Section] Comparison Operators
	
		let juan = 'juan';
		// Equality Operator (==) - checks whether the operand are equal/have the same content/value.
		//			- Returns a Boolean value
		// 			- Attempts to CONVERT AND COMPARE operands of different data types
		console.log(1 == 1); //true
		console.log(1 == 2); //false
		console.log(1 == '1'); //true
		console.log(0 == false); //true
		
		// compare two strings that are the same
		console.log('juan' == 'juan'); //true
		// case-sensitive for string
		console.log('JUAN' == 'juan'); //false
		console.log(juan =='juan'); //true

		// InEquality Operator (!=) - checks whether the operand are not equal/have the same content/value.
		//			- Returns a Boolean value
		// 			- Attempts to CONVERT AND COMPARE operands of different data types

		console.log(1 != 1); //false
		console.log(1 != 2); //true
		console.log(1 != '1'); //false
		console.log(0 != false); //false
		console.log('JUAN' != 'juan'); //true
		console.log(juan != 'juan'); //false

		// Strict Equality Operator (===) - checks whether the operand are equal/have the same content/value.
		//			- Returns a Boolean value
		//			- Also COMPARE the DATA TYPES of two values
		// 			- Attempts to CONVERT AND COMPARE operands 

		console.log(1 === 1); //true
		console.log(1 === 2); //false
		console.log(1 === '1'); //false
		console.log(0 === false); //false
		console.log('JUAN' === 'juan'); //false
		console.log(juan === 'juan'); //true

		// Strictly Inequality Operator (!==) - checks whether the operand are not equal/have the same content/value.
		//			- Returns a Boolean value
		//			- Also COMPARE the DATA TYPES of two values
		// 			- Attempts to CONVERT AND COMPARE operands of different data types

		console.log(1 !== 1); //false
		console.log(1 !== 2); //true
		console.log(1 !== '1'); //true
		console.log(0 !== false); //true
		console.log('JUAN' !== 'juan'); //true
		console.log(juan !== 'juan'); //false

// [Section] Relational Operators - Checks whether one value is 	greater or less than to the other values
	
		let a = 50;
		let b = 65;

		// GT / Greater than Operator (>)
		let isGreaterThan = a > b; //false
		// LT / Lesser than Operator (<)
		let isLessThan = a < b; //true
		// GTE or Greater than or Equal (>=)
		let isGTorEqual = a >= b; //false
		// LTE or Less than or Equal (<=)
		let isLTorEqual = a <= b; //true

		console.log(isGreaterThan);
		console.log(isLessThan);
		console.log(isGTorEqual);
		console.log(isLTorEqual);

		let numStr  = "30";
		console.log(a > numStr); //true
		console.log(b <= numStr); //false

		let strNum = "twenty";
		console.log(b >= strNum); //false
		// since the string is not numeric, the string was converted to a number and it resulted to NaN - (Not a Number);

// [Section] Logical Operator

		let isLegalAge = true;
		let isRegistered = false;

		// Logical AND operator (&& - double ampersand) - returns true if ALL OPERANDs is TRUE

		let allRequirementsMet = isLegalAge && isRegistered; //false
		console.log("Result of logical AND operator: "+allRequirementsMet);

		// Logical OR Operator (|| - Double Pipe) - returms true if one of the operands are true
		let someRequirementsMet = isLegalAge || isRegistered;
		console.log("Result of logical OR operator: "+someRequirementsMet);

		// Logical NOT Operator (! - Exclamation Point) - returns the opposite value

		let someRequirementsNotMet = !isRegistered;
		console.log("Result of logical NOT Operator: "+ someRequirementsNotMet);
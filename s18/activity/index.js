//console.log("Hello World!");
/*
	
	1. Create a function called addNum which will be able to add two numbers.
		- Numbers must be provided as arguments.
		- Return the result of the addition.
	   
	   Create a function called subNum which will be able to subtract two numbers.
	    - Numbers must be provided as arguments.
	    - Return the result of subtraction.

	    Create a new variable called sum.
	     - This sum variable should be able to receive and store the result of addNum function.

	    Create a new variable called difference.
	     - This difference variable should be able to receive and store the result of subNum function.

	    Log the value of sum variable in the console.
	    Log the value of difference variable in the console.
*/	
//add
	let add1= 5;
	let add2= 15;
	function addNum(){
		let sum = add1+add2;
		return sum;
	}
	console.log("Displayed sum of "+add1+" and "+add2);
	console.log(addNum(add1,add2));
//sub
	let sub1= 20;
	let sub2= 5;
	function subNum(){
		let difference = sub1-sub2;
		return difference;
	}
	console.log("Displayed difference of "+sub1+" and "+sub2);
	console.log(subNum(sub1,sub2));



/*
	2. Create a function called multiplyNum which will be able to multiply two numbers.
		- Numbers must be provided as arguments.
		- Return the result of the multiplication.

		Create a function divideNum which will be able to divide two numbers.
		- Numbers must be provided as arguments.
		- Return the result of the division.

		Create a new variable called product.
		 - This product variable should be able to receive and store the result of multiplyNum function.

		Create a new variable called quotient.
		 - This quotient variable should be able to receive and store the result of divideNum function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.
*/
//multi
	let mul1= 50;
	let mul2= 10;
	function multiplyNum(){
		let product = mul1*mul2;
		return product;
	}
	console.log("The product of "+mul1+" and "+mul2+":");
	console.log(multiplyNum(mul1,mul2));
//div
	let div1= 50;
	let div2= 10;
	function divideNum(){
		let quotient = div1/div2;
		return quotient;
	}
	console.log("The product of "+div1+" and "+div2+":");
	console.log(divideNum(div1,div2));


/*
	3. Create a function called getCircleArea which will be able to get total area of a circle from a provided radius.
		- a number should be provided as an argument.
		- look up the formula for calculating the area of a circle with a provided/given radius.
		- look up the use of the exponent operator.
		- return the result of the area calculation.

		Create a new variable called circleArea.
		- This variable should be able to receive and store the result of the circle area calculation.
		- Log the value of the circleArea variable in the console.
*/
	let rad = 15;
	function getCircleArea(){
		
		let circleArea = Math.PI /*3.1416*/*rad**2;
		return circleArea.toFixed(2);
			}
	console.log("The result of getting the area of a circle with "+rad+" radius:");
	console.log(getCircleArea(rad));

/*	
	4. Create a function called getAverage which will be able to get total average of four numbers.
		- 4 numbers should be provided as an argument.
		- look up the formula for calculating the average of numbers.
		- return the result of the average calculation.
		
		Create a new variable called averageVar.
		- This variable should be able to receive and store the result of the average calculation
		- Log the value of the averageVar variable in the console.
*/	let a = 20;
	let b = 40;
	let c = 60;
	let d = 80;
	function getAverage(){
		let averageVar = (a+b+c+d)/4
		return averageVar;
	}
	console.log("The average of "+a+","+b+","+c+" and "+d+":");
	console.log(getAverage(a,b,c,d));
/*

	5. Create a function called checkIfPassed which will be able to check if you passed by checking the percentage of your score against the passing percentage.
		- this function should take 2 numbers as an argument, your score and the total score.
		- First, get the percentage of your score against the total. You can look up the formula to get percentage.
		- Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
		- return the value of the variable isPassed.
		- This function should return a boolean.
*/	let value = 38;
	let total = 50;
	function checkIfPassed(){
		let percent = (value/total)*100;
		let isPassed = percent >= 75;
		return isPassed;
	}
	console.log("Is "+value+"/"+total+" a passing score?");
	console.log(checkIfPassed(value,total));



/*
		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/


//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
	module.exports = {

		addNum: typeof addNum !== 'undefined' ? addNum : null,
		subNum: typeof subNum !== 'undefined' ? subNum : null,
		multiplyNum: typeof multiplyNum !== 'undefined' ? multiplyNum : null,
		divideNum: typeof divideNum !== 'undefined' ? divideNum : null,
		getCircleArea: typeof getCircleArea !== 'undefined' ? getCircleArea : null,
		getAverage: typeof getAverage !== 'undefined' ? getAverage : null,
		checkIfPassed: typeof checkIfPassed !== 'undefined' ? checkIfPassed : null,

	}
} catch(err){

}
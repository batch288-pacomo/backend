// console.log("Hello World!");

	function printInput(){
		let nickname = "Chris";

		console.log("Hi, "+nickname);
	}

	printInput();
	printInput();

	// For other cases, function can also process data directly passed into it instead of relying only on Global Variables

		function printName(name){
			console.log("My name is "+name);
		}

		printName("Juana");

		printName("Chris");

	// Variables can also be passed as an argument

		let sampleVariable = "Curry";

		let sampleArray = ["Davis", "Green","Jokic","Tatum"]

		printName(sampleVariable);
		printName(sampleArray[1]);

	// One example of using the Parameter and Argument
		// functionName(number);

	function checkDivisibilityBy8(num){
		let remainder = num%8;

		console.log("The remainder of "+num+" divided by 8 is: "+remainder);
		let isDivisibleBy8 = remainder === 0;
		console.log("Is "+num+" divisible by 8?");
		console.log(isDivisibleBy8);
	}

	checkDivisibilityBy8(64);

	// Functions as Argument
		// Function parameters can also accept other functions as arguments

	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed!");
	}

	function invokeFunction(func){
		func();
	}

	invokeFunction(argumentFunction);

	// Function as Argument with return keyword

	function returnFunction(){
		let name = "Chris";
		return name;
	}

	function invokedFunction(func){
		console.log(func());
	}

	invokedFunction(returnFunction);

	// Using multiple parameters
	// Multiple "arguments" wil correspond to the number of "parameters" declared in a function in succeeding order

	function createFullName(firstName, middleName, lastName){
		console.log(firstName+" "+middleName+" "+lastName);
	}

	createFullName("Lloyd Carlo","Gutierrez","Pacomo");

	// In JavaScript, providing more/less arguments than the expected will not return an error

	// Using variable as Multiple arguments

	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	createFullName(firstName,middleName,lastName);

	// alert() - allows us to show a small window at the top of our browser page to show information to our users. As opposed to console.log

	//alert("Hello World!"); // will run immediately when the page loads
	// Syntax: alert("messageInString");

	function showSampleAlert(){
		alert("Hello, user!");
	}
	//showSampleAlert();
	//console.log("I will only log in the console when the alert is dismissed");

	// prompt() - datatype string - allows us tp show a small window at the top of the browser to gather user input 
	// Syntax: prompt("dialogInString");

	let samplePrompt = prompt("Enter your name: ");
	//console.log("Hello, "+samplePrompt);
	//console.log(typeof samplePrompt);

	let age = prompt("Enter your age: ");
	//console.log("Your age: "+age);
	//console.log(typeof age);

	// Returns an empty string when there is no input or null if the user cancels the prompt
	//let sampleNullPrompt = prompt("Don't enter anything: ");
	//console.log(sampleNullPrompt);

	function printWelcomeMessage(){
		let firstName = prompt("Enter your first name: ");
		let lastName = prompt("Enter your last name: ");
		console.log("Hello, "+firstName+" "+lastName+"!");
		console.log("Welcome to my page!");
	}
	printWelcomeMessage();

	
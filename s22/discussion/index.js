//console.log("Hello Jesus");


// Array Methods - Javascript has build-in functions and methods for arrays. This allows us to manipulate and access array items
// Mutator Method - are functions that "mutate" or change an array after they're created
// 		-These methods manipulate the original array performing various such as adding or removing elements

let fruits = ["Apple","Orange","Kiwi","Dragon Fruit"];

// push() - add an elememt in the end of an array and returns the updated array's length
/*
	Syntax:
		arrayName.push();
*/
console.log("Current array: ");
console.log(fruits);

let fruitsLength = fruits.push("Mango");

console.log(fruitsLength);
console.log("Mutated array from push method: ");
console.log(fruits);

		// Pushing multiple elements to an array

fruitsLength = fruits.push("Avocado","Guava");
console.log(fruitsLength);
console.log("Mutated array after pushing multiple elements:");
console.log(fruits);

// pop() - removes the last element AND returns the removed elements
/*
	Syntax:
		arrayName.pop();
*/

console.log("Current Array: ");
console.log(fruits);

let removedFruit = fruits.pop();

console.log(removedFruit);
console.log("Mutated array from the pop method:");
console.log(fruits);

// unshift() - it adds one or more elements at the beginning of an array AND it returns the update array length
/*
	Syntax:
		arrayName.unshift("elementA");
		arrayName.unshift("elementA","elementB" . . .)
*/

console.log("Current Array:");
console.log(fruits);

fruitsLength = fruits.unshift("Lime","Banana");
console.log(fruitsLength);
console.log("Mutated array from unshift method:");
console.log(fruits);

// shift() - removes an element at the beginning of an array AND returns the removed element
/*
	Syntax:
		arrayName.shift();
*/
console.log("Current Array: ");
console.log(fruits);

removedFruit = fruits.shift();

console.log(removedFruit);
console.log("Mutated array from the shift method: ");
console.log(fruits);

// splice() - simultaneously removes an elements from a specified index number and adds element
/*
	Syntex:
		arrayname.splice(startingIndex, deleteCount,elementsToBeAdd)
*/

console.log("Current Array: ");
console.log(fruits);

fruits.splice(fruits.length,0,"Cherry");
console.log("Mutated array after the splice method: ");
console.log(fruits);

//sort() - Rearranges the array elements in alphanumeric order
/*
	syntax:
		arrayName.sort();
*/

console.log("Current Array: ");
console.log(fruits);

fruits.sort();

console.log("Mutated array from the sort method:");
console.log(fruits);

//reverse() - reverses the order of array elements
/*
	syntax:
		arrayName.reverse();
*/

console.log("Current Array: ");
console.log(fruits);

fruits.reverse();

console.log("Mutated array from the reverse method:");
console.log(fruits);






//[Section] Non - mutator methods - are functions that do not modify or change an array after the're created
// 		-These methods do not manipulate the original array performing various task such as returning elements from an array and combining arrays and printing the outputs

let countries=["US","PH","CAN","SG","TH","PH","FR","DE"];

// indexOf() - returns the index number of the first matching element found in an array
// 			- if no match was found, the result will be -1. The Search process will be done from the first element proceeding to the last element
/*
	Syntax:
		arrayName.indexOf(serchValue);
		arrayName.indexOf(serchValue,startingIndex);
*/

let firstIndex = countries.indexOf('PH');
console.log(firstIndex);

let invalidCountry = countries.indexOf("BR");
console.log(invalidCountry);

firstIndex = countries.indexOf('PH',2);
console.log(firstIndex);

console.log(countries);

// lastIndexOf()- returns the indexx number of the last matching elements found in an array
// 		-the search process will be done from last element proceeding to the first element
/*
	syntax:
		arrayName.lastIndexOf(serchValue);
		arrayName.lastIndexOf(serchValue,startingIndex);
*/

let lastIndex = countries.lastIndexOf('PH');
console.log(lastIndex);

invalidCountry = countries.lastIndexOf("BR");
console.log(invalidCountry);

lastIndexOf = countries.lastIndexOf('PH',6);
console.log(lastIndexOf);

console.log(countries);

// slice() - portions/slices elements from an array AND return a new array
/*
	syntax:
		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex,endingIndex);
*/
// Slicing off elements from a specified index to the last element

let slicedArrayA = countries.slice(2);
console.log("Result from slice method:");
console.log(slicedArrayA);

// Slicing off elements from a specified index to another index
console.log(countries);

let slicedArrayB = countries.slice(2,7);
console.log("Result from the slice method");
console.log(slicedArrayB);
// element staringIndex value and before endingIndex

// slicing off elements starting from the last element of an array:
console.log(countries);

let slicedArrayC = countries.slice(-3);
console.log("Result from the slice method");
console.log(slicedArrayC);

// toString()- returns an array as string separated by commas
/*
	syntax:
		arrayName.toString();
*/

let stringArray = countries.toString();
console.log("Result from toString Method:");
console.log(stringArray);
console.log(typeof stringArray);

// concat() - combines arrays to an array or elements and returns the combined result
/*
	syntax:
		arrayA.concat(arrayB);
		arrayA.concat(elementA);
*/

let taskArrayA = ["drink HTML", "eat javascript"];
let taskArrayB = ["inhale CSS", "breathe sass"];
let taskArrayC = ["get git", "be node"];

let tasks = taskArrayA.concat(taskArrayB);
console.log(tasks);

let combinedTask = taskArrayA.concat("smell express","throw react");
console.log(combinedTask);

// concat multiple array into an array

let allTasks = taskArrayA.concat(taskArrayB,taskArrayC);
console.log(allTasks);

// concat array to an array and element
let exampleTask = taskArrayA.concat(taskArrayB,"smell express");
console.log(exampleTask);

// join() - returns an array as string separated by specified separator string
/*
	syntax:
		arrayName.join("separatorString")
*/


let users = ["John","Jane","Joe","Robert"];

console.log(users.join()); //default
console.log(users.join(""));	//separatorString
console.log(users.join(" * "));	//separatorString






//[Section] Iteration Methods - are loops designed to perform repetitive task
// 	- iteratuin method loop over all items in an array

// forEach() - similar to a for loop that iterates on each of array element
/*
	syntax:
		arrayName.forEach(function(
		indivElement){statement};)
*/

console.log(allTasks);
// 


allTasks.forEach(function(task){
	console.log(task);
});

// filteredTask variable will hold all the elements from the allTasks array that has more than 10 characters

let filteredTasks = [];

allTasks.forEach(function(task){
	if(task.length > 10){
		filteredTasks.push(task);
	}
})
console.log(filteredTasks);

//map() - iterates on each element and returns new array with different values depending on the result of the function's operation

let numbers = [1,2,3,4,5,];
let numberMap = numbers.map(function(number){
	//return number * number;
	return number + 1;
})
console.log(numbers);
console.log(numberMap);

// every() - will check if all elements in an array meet the given condition. LIKE AND
// 		-will return true value if all elements meet the condition and false otherwise
/*
	syntax:
		let/const resultArray = arrayName.every(function(indivElement){return expressopn/condition})
*/

numbers = [1,2,3,4,5,];

let allValid = numbers.every(function(number){
	//return(number<3);
	return(number<6);
})
console.log(allValid);

// some() - checks if at least one element in the array meets the given condtion. LIKE OR
/*
	syntax:
		let/const resultArray = arrayName.some(function(indivElement){return expression/condition;})
*/

let someValid = numbers.some(function(number){
	return (number < 2);
})
console.log(someValid);

//filter() - returns new array that contains elements which meets the given
// 		- returns an empty array if no elements were found
/*
	syntax:
		let/const resultArray = arrayName.filter(
		function(indivElement){
			return expression/condtion;
		})
*/
numbers = [1,2,3,4,5,];
let filterValid = numbers.filter(function(number){
	return(number % 2 === 0);
})
console.log(filterValid);

// includes() - check if the argument passed can be found in the array

let products = ["Mouse","Keyboard","Laptop","Monitor"];
let productsFound1 = products.includes("Mouse");
console.log(productsFound1);

// reduce() - evaluates elements from left to right and returns/reduces the array into single value
// 1st parameter in the function = accumulator
// 2st parameter in the function = current value
console.log(numbers = [1,2,3,4,5,]);

let reducedArray = numbers.reduce(function(x,y){
	console.log("Accumulator: "+x);
	console.log("current value: "+y);
	//return x+y;
	return x+y;
})

console.log(reducedArray);

products = ["Mouse","Keyboard","Laptop","Monitor"];

let reducedArrayString = products.reduce(function(x,y){
	return(x+y);
})
console.log(reducedArrayString);

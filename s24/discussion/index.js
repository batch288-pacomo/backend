//console.log("Hello Jesus");

//[Section] Exponent Operator
//-Before the ES6 update
// 	-Math.pow(base, exponent);
const firstNum = Math.pow(8,2);
console.log(firstNum);
const secondNum = Math.pow(5,5);
console.log(secondNum);

//After the ES6
const thirdNum = 8**2;
console.log(thirdNum);
const fourthNum = 5**5;
console.log(fourthNum);

// [Section] Template Literals
// 		- It will allows us to write strings without using the concatenation operator
// 		- Greatly helps with the code readbility

// Before ES6 Update
let name = "John";
let message = 'Hello '+name+"! Welcome to programming!";
console.log(message);

//After the ES6 update
// 	-Uses backticks(``)
message = `Hello ${name}! Welcome to programming!`;
console.log(message);
const anotherMessage = `${name} attended a math competition.
He won it by solving the problem 8**2 with the
solution of ${firstNum}`;
console.log(anotherMessage);

const interestRate = .1;
const principal = 1000;

// Template literals allow us to write strings with embedded JavaScript expressions
// expressions are any valid unit of code that resolves to a value
// `${}` are used to include JavaScript expression in strings using the template literals
console.log(`The interest on your savings account is : ${interestRate*principal}`);

// [Section] Array Destructuring - allows us to unpack elements in array into distinct variables
/*
	syntax:
		let/const [variableNameA,variableNameB,variableNameC] = arrayName;

*/
const fullName = ["Juan","Dela","Cruz"];
// Before the ES6 Update
let firstName = fullName[0];
let middleName = fullName[1];
let lastName = fullName[2];
console.log(`Hello ${firstName} ${middleName} ${lastName}!
It's nice to meet you.`);
// After the ES6 update

let [name1,name2,name3] = fullName;

console.log(name1);
console.log(name2);
console.log(name3);

/*nickName = "Pedro";
console.log(nickName);*/

// Another example
let gradesPerQuarter = [98,97,95,94];
console.log(gradesPerQuarter);
let[firstGrading,secondGrading,thirdGrading,fourthGrading] = gradesPerQuarter;

firstGrading = 100;
console.log(firstGrading);

console.log(firstGrading);
console.log(secondGrading);
console.log(thirdGrading);
console.log(fourthGrading);

// [Section] Object Destructuring
//		-allows us to unpack properties of objects into distinct variables;
// 		-shortens the syntax for accessing properties from objects
/*
	syntax: 
		let/const{propertyNameA,propertyNameB . .} = objectName;
*/

//Before the ES6 update
	const person = {
		givenName: "Jane",
		maidenName: "Dela",
		familyName: "Cruz"
	};
	console.log(person);

	/*const givenName = person.givenName;
	const maidenName = person.maidenName;
	const familyName = person.familyName;

	console.log(givenName);
	console.log(maidenName);
	console.log(familyName);*/

// After the ES6 update
	const{maidenName,givenName,familyName} = person;

	console.log(`This is the givenName ${givenName}`);
	console.log(`This is the maindenName ${maidenName}`);
	console.log(`This is the familyName ${familyName}`);

// [Section] Arrow function
// 		- compact alternative syntax to a traditional functions
/*
	syntax:
		const/let variableName = () => {
			statements/codeblock;
		}
*/
	// arrow function without parameter
	const hello = () =>{
		console.log("Hello World from the arrow function")
	}
	hello();

	// arrow function with parameter

	const printFullName = (firstName, middleInitial,lastName) =>{
		console.log(`${firstName} ${middleInitial} ${lastName}`)
	}
	printFullName("John","D.","Smith");

	// Arrow function can also be used to loops
	const students = ["John","Jane","Judy"];

	students.forEach((students) => {
		console.log(`${students} is a student`)
	})

//[Section] Implicit return
	// example
	// if the function will run one line or one statement the arrow function will implicitly return the value
	const add = (x,y) => x+y;

	let total = add(10,12);

	console.log(total);

	function trialFunction(x,y) {return x+y};

	console.log(trialFunction(1,2));

// [Section] Default function Argument value
	// provides a default function argument value if none is provided when the function is invoked

	const greet = (name = "User",age = 0) => {
		return `Good morning, ${name}! I am ${age} years old`;
	}
	console.log(greet(undefined, 78));

	function addNumber(x=0,y=0){
		console.log(x);
		console.log(y);
		return x+y;
	}
	let sum = addNumber(y=1);
	console.log(sum);

// [Section] Class - Based Object Blueprints
// 		- allows us to create/instantiate of object using a class as blueprint

/*
	Syntax: 
		class className{
			constructor(objectPropertyA,
				objectProperyB){
					this.objectPropertyA = objectValueA;
					this.objectPropertyB = objectValueB;
				}
		}
*/

class Car {
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;

		this.drive = () => {
			console.log('The car is moving 150 km per hour.')
		}
	}
}

// Instantiate an Object

const myCar = new Car("Ford","Range",2021);

console.log(myCar);

myCar.drive();
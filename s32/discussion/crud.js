let http = require("http");

// mock database
let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "Jobert",
		"email": "jobert@mail.com"
	}
]

http.createServer(function(request, response) {

	// GET method
	if(request.url == "/users" && request.method == "GET") {
		// 'application/json' - sets response output to JSON data type
		response.writeHead(200, {'Content-Type': 'application/json'});
		// response.write() - method in Node.js that is used to write data to the response body in a HTTP server
		// JSON.stringify() - method converts the string input to JSON 
		response.write(JSON.stringify(directory));
		response.end();
	};

	// POST Method
	if (request.url == "/addUser" && request.method == "POST") {

		// Declare and initialize a "requestBody" variable to an empty string
		let requestBody = "";

		// request.on() - event listener in Node.js that is used to handle incoming data in HTTP server
		// data - is received from the client and is processed in the "data" stream
		request.on('data', function(data){
			// Assigns the data retrieved from the data stream to requestBody
			requestBody += data;
		});

		// response end step - only runs after the request has completely been sent
		request.on('end', function() {
			// Converts the string requestBody to JSON
			requestBody = JSON.parse(requestBody);

			// Create a new object representing the new mock database record
			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			}

			// Add the new user into the mock database
			directory.push(newUser);
			console.log(directory);

			response.writeHead(200, {'Content-Type': 'application/json'});
			response.write(JSON.stringify(newUser));
			response.end();
		});
	}

}).listen(4000)

console.log('Server running at localhost: 4000')
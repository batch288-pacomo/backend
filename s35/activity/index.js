const express = require("express");

// mongoose is a package that allows creation of schemas to model our data structures
// also has access to a number of methods for manipulating our database
const mongoose = require("mongoose");

const port = 3001;

const app = express();
	// [Section] MongoDB Connection
	/*
		- Connect to the database by passing your connection string
		- due to update in Mongo DB drivers that allow connection, the default connection string is being flagged as an error
		- by default the warning will be displayed in the terminal when the application is running
		-we add {useNewUrlParser:true}

		syntax:
		mongoose.connect("MongoDB string",
		{useNewUrlParser:true});
	*/

		mongoose.connect("mongodb+srv://admin:admin@batch288pacomo.ccdsv9l.mongodb.net/batch288-todo?retryWrites=true&w=majority",{useNewUrlParser: true});

		//Notification whether we connected properly with the database
		let db = mongoose.connection;
		// for catching the error just in case we had an error during the  connection

		// console.error.bind - allows us to print errors in the browser and in the terminal
		db.on("error",console.error.bind(console, "Error! Can't connect to the Database"));

		// if the connection is successful:
		db.once("open", ()=> console.log("We're connected to the cloud database!"));

	// [Section] Mongoose Schemas - schemas determine the structure of the document to be written in the database
	// Schemas - act as blueprint to our data
	// use the Schema() constructor of the mongoose module to create a new Schema object

		// Middlewares

		app.use(express.json()); // allows our app to read json data

		app.use(express.urlencoded({extended:true})); // allows our app to read data from froms

		/*app.listen(port,() => {
			console.log(`Server is running at port ${port}!`)
		})*/

// [Section] Models - uses schema and are used to create/instantiate objects that corresponse to the schema

		const taskSchema = new mongoose.Schema({
			//Define the fields with the corresponding data type
			name: String,
			// let us add another field which is status
			status: {
				type: String,
				default: "pending"
			}
		})

	
	// Models use schema and they act as the middleman from the server(JScode) to our database
	// to create a mode we will use model()

		const Task = mongoose.model('Task',taskSchema);

	// [Section] Routes
		// Create a POST route to create a new task
		/*
		Create a new task
			Business Logic:
				1. add a functionality to check whether there are duplicate task
				-	if the task is existing in the database, we return an error (the task is already existing)
				-	if the task doesnt exist in the database, we add it in the database
				2. The task data will be coming from the request's body
		*/
		app.post("/tasks",(request,response) => {
			// "findOne" method is a mongoose method that acts similar to "find" in MongoDB
			// if the findOne finds a documents that matches the criteria it will return the object/documents and if there's no object that matches the criteria it will return an empty object or null
			console.log(request.body)
			Task.findOne({name: request.body.name})
			.then(result => {
				// we can use if statement to chech or verify whether we have an object found
				if(result !== null){
					return response.send("Duplicate task found!");
				} else {
					// Create a new task and save it to the database
					let newTask = new Task({
						name: request.body.name
					})
					// The save() method will store the information to the database, since the newTask was created from the Mongoose Schema and Task Model it will be save in tasks collection
					newTask.save()
					return response.send("New task created!")

				}
			})
		})

// Get all the tasks in our colletion
/*
	1.Retrieve all the documents
	2.if an error is encountered, print the error
	3.if no error/s found, send a success status to the client and show the documents retrive
*/

		app.get("/tasks", (request,response) => {
			//find() method is a mongoose method that is similar to mongoDB find
			Task.find({}).then(result => {
				return response.send(result);
			}).catch(error => response.send(error));
		})


// Activity starts HERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


const activitySchema = new mongoose.Schema({
	username: {type: String},
	password: {type: String}
})

const Activity = mongoose.model('activity',activitySchema);

app.post("/signup",(request,response) => {
	console.log(request.body)
	Activity.findOne({username: request.body.username})
	.then(result => {
		if(result !== null){
			return response.send("Duplicate username found");
		} else { if(request.body.username !== "" && 
				request.body.password !== ""){
			let newActivity = new Activity({
				username: request.body.username,
				password: request.body.password
			})
			
			newActivity.save()
			.then(save => {return response.send("New user registered")})
			.catch(error => {console.log(error);
				response.send("Error occured while saving user");
			});
			}
			 else {
				response.send("BOTH username and password must be provided")
			}			
		
	}})
})


if(require.main === module){
	app.listen(port,() => {
	console.log(`Server is running at port ${port}!`)
})	
}
module.exports = app;
// Use the "require" directive to load the express module/package
// It will allow use to access methods and function that will help us create easily our application or server
// "module" is a software component or part of a program that contains one or more routine

const express = require("express");

//create an application using express
// Creates an express application and stores this in a constant variable called app
const app = express();

// For our application server to run, we need a port to listen
const port = 4000;

// middlewares - is software that provides common services and capabilities to application outside of what's offere by the operating system
// allow our application to read json data
app.use(express.json());

// this one will allow us to read data from forms
// By default, information received from the url can only be received as a string or an array
// By applying the option of "extended: true" this will allow us to receive information in other data types such as an object which will use throughout our application
app.use(express.urlencoded({extended:true}));

//Tell our server to listen to the port
// If the port is accessed, we can run the server
// It will return a message confirming that the server is running in the terminal
/*app.listen(port, () => console.log(`Server running at port ${port}`));
*/
// [Section] Routes
// Express has methods corresponding to each http method
// This route expects to receive a GET request at the base URI "/"
app.get("/", (request, response) => {
	// once the route is accessed it will send a string response containing "hello world"

	// Compared to the previous session, .end uses the node JS module's method
	// .send method uses the expressJS module's method instead to send a response back to the cliend

	response.send("Hello Batch288!");
})
// This route expects to receive a GET request at the URI "/hello"
app.get("/hello",(request, response) => {
	response.send("Hello from the /hello endpoint");
})

/*app.get("/hello",function(request, response){
	response.send("Hello from the /hello endpoint");
})*/	

// This route expects to receive a POST request at the URI "/hello"
app.post("/hello",(request, response) => {
	//request.body - contains the contents/data of the request body
	// all the properties defined in our POSTMAN request will be accessible here as properties with the same name
	console.log(request.body);

	response.send(`Hello there ${request.body.lastname} ${request.body.middle} ${request.body.firstname}!`)
})

// An array will store user objects when the "/signup" route is accessed
// this will serve as our mock database

let users = [];
app.post("/signup", (request,response) =>{
	console.log(request.body);
	if(request.body.username !== ""&& request.body.password !== ""){
		users.push(request.body);

		response.send(`User ${request.body.username} successfully registered!`)
	}else{
		response.send('Please input BOTH username and password.');
	}
	/*response.send("Trial");*/
})


// This rout expect to recieve a PUT request at the URI "/change-password"
// This will update the password of a user that matches the information provided in the client/postman
app.put("/change-password",(request,response) => {

	let message;

	for(let index = 0; index < users.length; index++){
		// if the provided username in the client/postman and the username of the current object in the loop is the same
		if(request.body.username == users[index].username){
				users[index].password = request.body.password

				message = `User ${request.body.username}'s password has been updated!`

				break;
		} else {
			message = 'User does not exist!'
		}
	}

	response.send(message);
})

if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));
}
module.exports = app;


app.get('/home',(request,response) => {
	response.send("Welcome to the home page")
})

/*app.get('/users',(request,response) => {
	for(let index = 0; index < users.length; index++){
		response.send(users[index])
	}
})*/

app.get('/users',(request,response) => {
		response.send(users)
	
})

/*app.delete('/delete-user',(request,response) => {
	let message1 = "";
	for(let index = 0; index < users.length; index++){
	
		if(request.body.username == users[index].username){
		let user = users[index].username
			users.splice(0,index)
			message1 = `User ${user} has been deleted`
				break;
		}
		if(message1 === undefined){
			message1 = `User does not exist`

		} else {
			message1 = `No users found`
		}
	}
	response.send(message1);
})*/

app.delete('/delete-user',(request,response) => {
	let message;

	if(users!=""){
		for(let index = 0; index < users.length; index++){
			if(request.body.username == users[index].username){
				users.splice(index,1);
				message = `User ${request.body.username}'s has been deleted`
				break;
			}
		}
		if(message === undefined) message = `users does not exist`
	}
	else message = `No users found`

	response.send(message);

});
// single line comment
/*
this is a 
multi-line 
comment
*/

// [Section] Syntax, Statements and Comments
// Statements in programming - these are the instructions that we tell the computer to perform
//JavaScript Stament usually it ends with semicolon(;)
// Semicolons are not required in JS, but we will use it to help us prepare for the other stric languages live Java.
// Synta in programming - it is the set of rules that describes how statements must be constructer
// All lines/blocks of code should be written in a specific or else the statement will not run

// [Section] Variables
// It is used to contain/store date
// Any information that is used by an application is stored in what we call memory
// When we create variables, certain portion of a device memory is given a "name" that we call "Variables"
// Declaring Variables - tells our devices that a variable name is created and is ready to store data
// Syntax:
	// let/const variableName
let myVariable;
// by default if you declare a variable and did not initialize its value it will become "undefined"

// console.log() is useful for printing values of a variable or certain result of code into the Google chromes Browser's console
console.log(myVariable);

/*
	Guides in writing variables:
	1. Use the 'let' keyword followed by the variable name of your choice and use the assignment operator(=) to assign a value.
	2. variable names should start with a lowercase character, use nameCase for multible words.
	3. For constant variable, use 'const' keyword.
	4. Variable names, it should be indicative(descriptive) of the value being stored to avoid confusion
*/

// Declaring and initializing Variables
// Initializing variables - the instance when a variable is given it's initial or starting value
	// Syntax
		// let/const variableName = value;

// example:
let productName = 'desktop computer'

console.log(productName);

let productPrice = 18999;
console.log(productPrice);

// In the context of certain applications, some variables/information are constant and should not change
// In this example, the interest rat for loan or savings account or a mortagage must not change due to real world concerns.

const interest = 3.539;
console.log(interest);

// Reassigning Variable Values - means changing it's initial or previous into another value

	// Syntax
	//  variableName = newValue

productName = 'Laptop';
console.log(productName);

// The value of a variable declared using the const keyword can't be reassigned

/*interest = 4.489;
console.log(interest);*/

// String can use single/double qout "",''
// Declaring,Initializing,Reassigning

// var vs let/const keyword
	// var - is also used in declaring variables. but var is an EcmaScript 1 version (1997)

// What makes let/const different from var?
	// There are issues associated with variables declared/ creasted using var, regarding hoisting
	// Hoisting is JavaScript default behavior of moving declarations to the top
	// In terms if variables and constants, keyword var is hoisted and let and const does not allow hoisting

// Example of Hoisted:

a = 5;
console.log(a);
var a;

/*b = 6;
console.log(b);
let b;*/

// let/const local/global scope
	// Scope essentially means where these variables are available or accessible for use
// let and const are block scoped
// block is a chunk of code bounded by{}. A block lives in an curly braces. Anything within the braces are block

let outerVariable = "Hello";
let globalVariable;
	{	
		let innerVariable = 'hello again';
		console.log(innerVariable);
		console.log(outerVariable);
		globalVariable = outerVariable;
	}
console.log(globalVariable);

// Multiple variable declarations and Initialization
// Multiple variables may be declared in one statement

let productCode = "DC017", productBrand = "Dell";
console.log(productCode);
console.log(productBrand);

// Multiple variables to be consoled in one line.

console.log(productCode, productBrand);

// Using a variable with a reserved keyword cannot be used as a variable name as it has function in Javascript

// const let = "Hi Im let keyword";
// console.log(let);

// Strings - series of characters that creat a word, a phrase, a sentence or anything related to creating text
// String in javaScript can be written using either a singe('') or double("") quote

let country = 'Philippines';
let province = "Metro Manila";
	// Concatenation of strings in Javascript 
		// Multiple string values can be combined to create a single string using the "+" symbol
let fullAddress = province +','+ country;
console.log(fullAddress);

let greeting = 'I live in the '+ country;
console.log(greeting);

// escape characters (\) in combination with other characters can produce different effects/results.
// \n - this creates a next line in between text
let	 mailAddress = 'Metro Manila\n\nPhilippines';
console.log(mailAddress);

let	message = "John's employees went home early.";
console.log(message);

message = 'John\'s employees went home early.';
console.log(message);

// Numbers
// Intergers/Whole Numbers
let headcount = 26;
console.log(headcount);

// Decimal Number/Fractions
let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining text and strings
console.log("John's grade last quarter is " +grade);

// Boolean [true or false] values are normally used to create values relating to the state of certain things.
let isMarried = false;
let isGoodConduct = true;

console.log("isMarried: "+isMarried);
console.log("isGoodConduct:" +isGoodConduct);

// Arrays - are special kind of data that used to store multiple related value
// In javascript - can store different data types but is normally used to store similar data types
		//similar data types
	 	// Syntax:
			// let/const arrayName = [elementA,elementB,elementC,...];
		let grades = [98.7,92.1,90.2,94.6];
		console.log(grades);

// different data types - not recommended kind of use
		let details = ["John","Smith",32,true];
		console.log(details);

// Objects - are another special kind of data type that is use to mimic real world objects/items
		/*
			Syntax:
			let/const objectName = {
				propertyA: valueA,
				propertyB: valueB
			}
		*/
		let person = {
			fullName: "Juan Dela Cruz",
			age: 35,
			isMarried: false,
			contact:["+63 917 123 4567","8123 4567"],
			address:{
				houseNumber:'345',
				city:'Manila'
			}

		}
		console.log(person);

		// type of operator, is used to determine the type of data or value of a variable.

		console.log(typeof mailAddress);
		console.log(typeof headcount);
		console.log(typeof isMarried);
		console.log(typeof grades);

		// Note: Array is a special type of object with methods and function to manipulate.

		// Constant Objects and arrays
		/*
		The keyword const is a little misleading
		It does not define a constant value. It defines a constant reference to a value:

		Because of this you cannot:
			Reassign a constant value:
			Reassign a constant array:
			Reassign a constant object:

			But you can:

			Change the elements of a constant array
			CHange the properties of constant object
		*/

		const anime = ["One piece", "One Punch Man", "Attack on Titan"];
		/*anime = ["One piece", "One Punch Man", "kimetsu no yaiba"];
		console.log(anime);*/
		anime[2]= "Kimetsu no yaiba";
		console.log(anime);

		// Null - it is used to intentionally express the absence of a value in a variable.

		let spouse = null;
		spouse = "Maria";

		// Undefined - Represents the state of a variable that has been declared buy without an assigned value.

		let fullName;
		fullName = "Maria";
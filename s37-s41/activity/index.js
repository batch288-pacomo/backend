const express = require("express");
const mongoose = require("mongoose");
const cors = require('cors');

const usersRoutes = require("./routes/usersRoutes.js")
// cors - it will allow our backend application to be available to our frontend application
// 		- it will also allows us to control the app's Cross origin resource sharing settings
const coursesRoutes = require("./routes/coursesRoutes.js");

const port = 4001;

const app = express()

mongoose.connect("mongodb+srv://admin:admin@batch288pacomo.ccdsv9l.mongodb.net/CourseBookingAPI?retryWrites=true&w=majority",{useNewUrlParser:true,
	useUnifiedTopology:true
});

//db connections
const db = mongoose.connection;
	db.on("error", console.error.bind(console, "Error, can't connect to the db!"));
	db.once("open", () => console.log("We are now connected to the db!"));

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));
// cors reminder - we are going to use this for the sake of the bootcamp
app.use(cors())

// add the routing of the routes from the usersRoutes

app.use("/users", usersRoutes);

app.use("/courses", coursesRoutes);




//app listen
app.listen(port, () => console.log(`The server is running at port ${port}!`));

if(require.main === module){
	app.listen(process.env.PORT || 4000, () => {
	    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
	});
}

module.exports = {app,mongoose};

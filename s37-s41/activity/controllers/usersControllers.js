const Users = require("../model/Users.js");
const bcrypt = require("bcrypt");
const Courses = require("../model/Courses.js");
// Require the auth.js
const auth = require("../auth.js");

// Controllers
// registerUser
// Create a controller for the signup

/*
Business Logic/Flow
1. we have to validate whether the user is existing or not,check if the user is existing. validate whether the email exist on our databases or not
2. if user email is existing, we will prompt an error telling the user that the email is taken
3. otherwise, we will sign up or add the user in our database
*/

// find method: it will return an array of object that fits the given criteria

module.exports.registerUser = (request,response) => {
	Users.findOne({email: request.body.email})
	// find (result.length > 0)
	.then(result => {

		// we need to add if statement to verify whether the email exist in our already in our database
		// result.length > 0 (if Users.find)
		if(result){
			return response.send(false);
		}else{
			// Create a new Object instantiated using the Users model
			let newUser = new Users({
				firstName: request.body.firstName,
				lastName: request.body.lastName,
				email: request.body.email,
// hashSync () - it hash/encrypt our password
	// second argument salt rounds
				password: bcrypt.hashSync(request.body.password,10)/*request.body.password*/,
				isAdmin: request.body.isAdmin,
				mobileNo: request.body.mobileNo
			})
			// Save the User
			// Error handling when save()
			newUser.save()
			.then(saved => response.send(true))
			.catch(error => response.send(false));
		}
	})
	.catch(error => response.send(false));
}

// new controller for the authentication of the user
module.exports.loginUser = (request,response) => {

	Users.findOne({email: request.body.email})
	.then(result => {
		if(!result){
			return response.send(false)
		}else{
			// compareSync method is used to compare a non encrypted password from the login form to the encrypted password retrieve form the find method. returns true or false depending the result of the comparison
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

			if(isPasswordCorrect){
				/*return response.send("Login successful!")*/
				return response.send(
					{auth: auth.createAccessToken(result)}
				)
			}else{
				return response.send(false)
			}
		}
	})
	.catch(error => response.send(false));
}

// Activity code s38 //////////////////////////////////
module.exports.getProfile = (request,response) => {

	const userData = auth.decode(request.headers.authorization)

	/*console.log(userData);*/

	if(userData.isAdmin){
			Users.findOne({_id: request.body._id})
			.then(result => {
				
				password: result.password = ""
				
				return response.send(result);
			})
			.catch(error => {
				return response.send(error);
			})
		}else{
			return response.send("userControllers line 95")
		}
	}

// Controller for the enroll course
module.exports.enrollCourses = (request, response) => {
	const courseId = request.body.id;
	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		return response.send(false);
	}else{
		// Push going to user documents

	let isUserUpdated =	Users.findOne({_id: userData.id})
		.then(result => {
			result.enrollments.push({
				courseId : courseId
			});
			result.save()
			.then(saved => true)
			.catch(error => false)
		})
		.catch(error => false)

		// Push going to course documents

	let isCourseUpdated = Courses.findOne({_id: courseId})
		.then(result => {
			result.enrollees.push({userId: userData.id});
			result.save()
			.then(saved => true)
			.catch(error => false)

		})
		.catch(error => false)

	// if condition to check whether we updated the users document and courses document
		
		if(isUserUpdated&&isCourseUpdated){
			return response.send(true)
		} else {
			return response.send(false)
		}

	}
}



// OWN CODE FOR TESTING ///////////////////////////////////
//get all users data

module.exports.getAllUsers = (request,response) => {
	Users.find({})
	.then(result => {
		return response.send(result);
	})
	.catch(error => {
		return response.send(error);
	})
}

// ///////////////////////////////////////////////
module.exports.retrieveUserDetails = (request,response) => {
	const userData = auth.decode(request.headers.authorization);
	Users.findOne({_id: userData.id})
	.then(data => response.send(data))
	.catch(error => {
		return response.send(error);
	})
}
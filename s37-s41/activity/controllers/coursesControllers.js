const Courses = require("../model/Courses.js");

const auth = require("../auth.js");



//this controller is for the course creation
/*module.exports.addCourse = (request,response) => {
	// Create an object using the courses model
	let newCourse = new Courses(
// Create an object using the Courses model

	{
		name: request.body.name,
		description: request.body.description,
		price: request.body.price,
		isActive: request.body.isActive,
		slots: request.body.slots

	})

	newCourse.save()
	.then(save => response.send(true))
	.catch(error => response.send(false));
	}else{
		return response.send(false);
	}*/
module.exports.addCourse = (request, response) => {

    const userData = auth.decode(request.headers.authorization);

    if(userData.isAdmin){
        //Create an object using the Courses Model
        let newCourse = new Courses({
            //supply all the required fields declared on the Courses Model.
            name: request.body.name,
            description: request.body.description,
            price: request.body.price,
            isActive: request.body.isActive,
            slots: request.body.slots
        })

        newCourse.save()
        .then(save => response.send(true))
        .catch(error => response.send(false))
    }else{
        return response.send(false);
    }

}
/////////////////////////////////////////////

module.exports.createCourse = (request,response) =>{
	const userData = auth.decode(request.headers.authorization)
	if(userData.isAdmin){
		let makeCourse = new Courses(
				{
					name: request.body.name,
					description: request.body.description,
					price: request.body.price,
					isActive: request.body.isActive,
					slots: request.body.slots

				})
		makeCourse.save()
		.then(save => response.send("Course successfully created!"))
		.catch(error => response.send(error));
	}else{
		return response.send("user is not admin");
	}
}
// In this controller we are going to retrieve all the course in our database
// AllCourses
module.exports.getAllCourses = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	if(userData.isAdmin){
		Courses.find({})
		.then(result => response.send(result))
		.catch(error => response.send(false));
	}else{
		return response.send(false)
	}
}

// Route for retrieving all active courses
module.exports.getActiveCourses = (request, response) => {
	Courses.find({isActive: true})
	.then(result => response.send(result))
	.catch(error => response.send(false));
}

module.exports.getCourse = (request, response) => {
	const courseId = request.params.courseId;

	Courses.findById(courseId)
	.then(result => response.send(result))
	.catch(error => response.send(false))

}

// This controller will update our course detail
module.exports.updateCourse = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const courseId = request.params.courseId;

// description, price, name
	let updatedCourse = {
		name: request.body.name,
		description: request.body.description,
		price: request.body.price
	}
	if(userData.isAdmin){
		Courses.findByIdAndUpdate(courseId, updatedCourse)
		.then(result => response.send(true))
		.catch(error => response.send(false));
	}else{
		return response.send(false)
	}
}

//{new:true} - to show the updated docs



// Activity s40 //////////////////////////////////
// This controller will archive course
module.exports.archiveCourse = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const courseId = request.params.courseId;

	let archiveCourse = {
		isActive: request.body.isActive
	}
	if(userData.isAdmin){
		if(archiveCourse.isActive){
			Courses.findByIdAndUpdate(courseId, archiveCourse)
			.then(result => response.send(true))
			.catch(error => response.send(false));
		}/*else{
			Courses.findByIdAndUpdate(courseId, archiveCourse)
			.then(result => response.send(true))
			.catch(error => response.send(false));
		}*/
		
	}else{
		return response.send(false)
	}
	// false = archive, true = not archive
}




/*if(userData.isAdmin){
		Courses.findByIdAndUpdate(courseId, archiveCourse)
		.then(result => response.send(`${result.name}`))
		.catch(error => response.send(error));
	}else{
		return response.send(`You don't have access in this route!`)
	}*/
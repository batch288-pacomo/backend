const express = require("express");
const usersControllers = require("../controllers/usersControllers.js");
const auth = require("../auth.js");
const router = express.Router();

// Routes
router.post("/register", usersControllers.registerUser);

// login
router.post("/login", usersControllers.loginUser);



// tokenVerification
/*router.get("/verify", auth.verify);*/

// route for course enrollment
router.post("/enroll", auth.verify, usersControllers.enrollCourses);



// Activity code s38 //////////////////////////////////

// getProfile
router.get("/details", auth.verify, usersControllers.getProfile);
// userDetails
router.get("/userDetails", auth.verify, usersControllers.retrieveUserDetails);


// Own Create ///////////////////////////////////////////////
// getAllUser
router.get("/data", usersControllers.getAllUsers);


module.exports = router;
const express = require("express");
const auth = require("../auth.js");
const coursesControllers = require("../controllers/coursesControllers.js");

const router = express.Router();

// this route is responsible for adding course in our db

router.post("/addCourse", coursesControllers.addCourse);


router.post("/makeCourse", auth.verify, coursesControllers.createCourse);


// Route for retrieving all courses
router.get("/", auth.verify, coursesControllers.getAllCourses);



// Route for retrieving all active courses
router.get("/activeCourses", coursesControllers.getActiveCourses);

// Route for retrieving specific courses information

router.get("/:courseId", coursesControllers.getCourse);

// updating course

router.patch("/:courseId", auth.verify, coursesControllers.updateCourse);

// archive ////////Activity
router.patch("/archive/:courseId", auth.verify, coursesControllers.archiveCourse);

module.exports = router;
const jwt = require("jsonwebtoken");

// User defined string data that will be used to create our JSON web token.
// Used in the algorithm for encrypting our data which makes it difficult to decode the information without the defined keyword
const secret = "CourseBookingAPI";

// [Section] JSON web token - JWT is a way of securely passing information from the server to the frontend or to the parts of server
// Information is kept secure through the use of the secret code

// Function for token creation

// Analogy:
/*
	Pack the gift and provide a lock with the secret code as the key
*/

// the argument that will be passed in the parameter will be the document or object that contains the info of the user
module.exports.createAccessToken = (user) => {
	const data = {
		id : user._id,
		isAdmin : user.isAdmin,
		email : user.email
	}

	// Generate a JSON web token using JWT's sign method
	// generate the token using the form data and the secret code with no additional options provided
	return jwt.sign(data, secret, {});
}

// Token Verification
// Analogy
/*
	Receiving gift and open the lock to verify if the sender is legitimate and the gift was not tampered
*/

module.exports.verify = (request,response,next) => {
	/*console.log(request);*/

	let token = request.headers.authorization;
	//console.log(token);

	if(token !== undefined){
		token = token.slice(7, token.length);
// Validate the token busing the "verify" method in decrypting the token using the secret code
		return jwt.verify(token, secret, (error, data) =>{
			if(error){
				return response.send(false);
			}else{
				next();
				/*return response.send("The token is good!");*/
			}
		})
	}else{
		return response.send(false)
	}
}

// decode the encrypted token

module.exports.decode = (token) => {
	token = token.slice(7, token.length);
	// The decode() is used to obtaion the information from the JWT
	// {complete:true} option allows us to return addtional information from the JWT token

	return jwt.decode(token, {complete: true}).payload;
}
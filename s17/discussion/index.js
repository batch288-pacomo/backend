console.log("Hello Batch 288");

//Functions in JavaScript are lines/blocks of codes that tells our device/application to perform a certain task when called/ invoke
//  - are mostly created to create complicated tasks to run several lines of code in succession

	//Function declarations - defines a function with the specified parameters
	// Syntax:
		/*
			function functionName(){
				codeBlock/ Statements;
			}
		*/
	// function keyword - used to defined a JavaScript function
	// functionName - named to be able to use later in the code for the invocation
	// function block({}) - the statements which comprise the body of the function. This is where the code will be executed.

		// Example:
			function printName(){
				console.log("My name is John!");
			}
 // Function Invocation - execute the codeblock and 				statement inside the function
		// call a function/ invoke a function

		// Let's invoke/ call the function that we declare

		printName();

// Function Declaration vs. Expression
	// Function Declaratio - can be created through function declaration by using the function keyword and adding the function name

	// Declared function/s can be hoisted
		declaredFunction();
		// Note: Hoistin is a JavaScript behavior for certain variables and functions to run or use before their declaration
		function declaredFunction(){
			console.log("Hello World from declaredFunction");
		}

	// Function Expression - function can also be stored in a variable
	//			- function expression is an anonymous function assigned to a variable function

		/*function(){
			console.log("Hello Again");
		} //anonymous function*/

		let variableFunction = function(){
			console.log("Hello Again");
		} //variable function

		variableFunction();

		// we can also create a function expression of a named function

		let funcExpression = function funcName(){
			console.log("Hello from the other side!");
		};
		funcExpression();

		// you can reassign declared function and function expression to new anonymous function

		declaredFunction = function(){
			console.log("updated declaredFunction!")
		};
		declaredFunction();

		funcExpression = function(){
			console.log("Upadated funcExpression!");
		};
		funcExpression();

		// However, we cannot re-assign a function expression initialized with const.

		const constantFunc = function(){
			console.log("Initialized with const!")
			let x = 5;
		};
		 constantFunc();

		/*constantFunc = function(){
			console.log("cannot be re-assigned!");
		};

		constantFunc();*/

// Function Scoping - accessibility of variables within our program
		/*
			JavaScript variable has 3 types of scope
			1. Global
			2. local/block
			3 function
		*/
		{
			let localVar = "Armando Peres";
		}
		// localVar - being in a block, it will not be accessible to outside the block


		let globalVar = "Mr. Worldwide";

// Function Scope -
	// JavaScript has function scope: Each function creates a new scope
	// Variables defined inside a function are not accessible(visible) from outside the function

		function showNames(){
			const functionConst = "John";
			let functionlet = "Jane";

			console.log(functionConst);
			console.log(functionlet);
		}
		/*console.log(functionConst);
		console.log(functionlet);*/

// Nested functions
	// you can create another function inside a function. This is called nested function.

		function myNewFunction(){
			let name = 'Jane'
			// nested function
			function nestedFunction(){
				console.log(name);
			}

			nestedFunction();
		}

// Return Statement - allows us to output a value from a function to be passed to the line/block of code that invoked/called the function

		function returnFullName(){
			let fullName = "Jeffrey Smith Bezos";
			return fullName;
			console.log("Hello after the return keyword");

		}
		returnFullName();
		/*console.log(returnFullName());*/

		let fullName = returnFullName();
		console.log(fullName);

// Function Naming Conventions
		// Function names should be definitive of the task it will perform. It usually contains a verb.

		function getCourses(){
			let courses = ["Science 101","Math 101","English 101"];
			return course;
		}

		// Avoid generic names to avoid confusion within our code

		function get(){
			let name = "Jamie";
			return name;
		}

		// Avoid pointless and inappropriate function names

		function foo(){
			return 25%5;
		}

		// Name you functions in small caps. Follow camelCame when naming variables and functions with multiple words

		function displayCarInfo(){
			console.log("Brand: Toyota");
			console.log("Type: Sedan");
		}
// console.log("Hello Jesus");

// [Section] While Loop - takes an expression/condtion. Expressions are any unit of code that can be evaluated as true or false. If the condition evaluates to be a true, the statements/code block will be executed.
// 		- a loop will iterate a certain number of times until an express/condition is true.
// 		- Iteation is the tern given to repitition of statements.
/*
	Syntax:
		while(expression/condition){
				statement;
				increment/decrement; - stopper
		}
*/

let count = 5;
// While the value of count is not equal to zero the statement insise will run/iterate
	while(count !== 0){
		// current value of count is printed
		console.log("While: "+ count);
		// decreases the value of count by 1 after every iteration to stop the loop when it reaches 0
		count--;
	}

// [Section] Do While Loop - while loop works a lot like the while loop. But unlike while loops. do-while loops guarantees that the code will be executed at least once.

/*
	Syntax:
		do{
			statement;
			increament/decrement;
		}
		while(condition/expression)
*/
/*let number = Number(prompt("Give me a number:"));
	// Number function - works similarly as the "parseInt" function
	do{
		console.log("Do While: "+number)
		// increment
		number++;
	}
	while(number<10);*/

// [Section] - For Loop - for loop is more flexible than while and do-while loops. It consists of three parts:
// 		Initialization value - will track the progression of the loop
// 		Expression/Condition - will be evaluated which will determine whether the loop will run one more time
// 		Iteration - indicates how to advance the loop
/*
	Syntax:
		for(initializataion; expression; iteration){
			statement;
		}
*/
	

let myString = "alex";
// .length property - count the characters
console.log(myString.length);

let alex = myString.length;

for(let count=0; count<=alex; count++){
		console.log("The current value of count is "+count);
	}


/*let role = "hero";
	
	switch(role){
	case "human" : console.log("I'm a "+role2+" "+role);
	}

	let variable = "";
	console.log(!variable);*/ //Practice code


// Access elements of a string/ to the characters of the string

	console.log(myString[0]);
	console.log(myString[3]);


// We will create a loop that will print out the individual letters of the myString variable;

	for(let index = 0; index<myString.length;index++){
			console.log(myString[index]);
	}

// Create a string name "myName" with value of Alex;
let myName = "Alex";
// Create a loop that will print out the letter of the name individually and printout the number 3 instead when the to be printed is a vowel.

	for(let index = 0; index<myName.length; index++){
		console.log(myName[index]);
		if(	myName[index].toLowerCase() === "a" ||
			myName[index].toLowerCase() === "e" ||
			myName[index].toLowerCase() === "i" ||
			myName[index].toLowerCase() === "o" ||
			myName[index].toLowerCase() === "u"){
			
			console.log(3);

			} else{
			console.log(myName[index]);
		}
		}

let myNickName = "Chis Mortel";
let updatedNickName = myNickName.replace("Chis","Chris");

console.log(myNickName);
console.log(updatedNickName);

// [Section] Continue and Break Statement
//		- Continue - allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block
// 		- Break - statement is used to terminate the current loop once a match has been found

// Create a loop that if the count value is divided by and the remain is 0, it will print the number and continue to the next iteration of the loop

	for(let count = 0; count <= 20; count++){

		if(count>=10){
			console.log("The number is equal to 10, stopping the loop");
			break;
		}


		if(count % 2 === 0){
			console.log("The number is divisible to two, skipping . . .");
			continue;
		}
		console.log(count);
	}

// Create a loop that will iterate based on the length of the string name; 
// We are going to console the letters per line, and if the letter is equal to "a" we are going to console "Continue to the next iteration" then add continue statement
// If the current letter is equal to "d" we are going to stop the loop
console.log("_________________");
	let name = "alexandro";
	for(let index = 0; index < name.length; index ++){
		if(name[index] === "a"){
			console.log("Continue to the next iteration");
			continue
		} 
		else if(name[index] =="d"){
			break;
		} 
		else{
			console.log(name[index]);
		}
	}